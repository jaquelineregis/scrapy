# -*- coding: utf-8 -*-
import scrapy
import re

from decouple import config
from scrapy import signals
from scrapy.mail import MailSender
from scrapy_selenium import SeleniumRequest


class ProductsSpider(scrapy.Spider):
    name = 'products'
    data_discount = {}

    def start_requests(self):
        urls = [
            'https://www.multilaser.com.br/mouse-sem-fio-24-ghz-litio-preto-multilaser-mo277/p',
            'https://www.multilaser.com.br/pilhas-recarregaveis-aa-multilaser-2500mah-com-4-unidades-cb052/p',
            'https://www.multilaser.com.br/pilhas-recarregaveis-aaa-multilaser-1000mah-com-2-unidades-cb051/p',
            'https://www.multilaser.com.br/relogio-smartwatch-paris-atrio-preto-es267/p',
            'https://www.multilaser.com.br/descanso-para-os-pes-multilaser-ac059/p',
            'https://www.multilaser.com.br/combo-teclado-e-mouse-super-multimidia-usb-multilaser-tc215/p',
            'https://www.multilaser.com.br/mochila-swisspack-safe-multilaser-bo426-/p',
            'https://www.multilaser.com.br/descanso-de-pes-multilaser-ac279/p',
            'https://www.multilaser.com.br/over-ear-wireless-stereo-audio-ph150/p',
            'https://www.multilaser.com.br/oximetro-de-pulso-multilaser-hc023-/p',
        ]
        for url in urls:
            yield SeleniumRequest(
                url=url,
                callback=self.parse, 
                wait_time=60,
            )

    def parse(self, response):
        self.log('selo-frete-desconto')
        name = response.selector.xpath('//*[@class="nameProduct"]/div/text()').extract_first()
        best_price = response.selector.xpath('//*[@class="skuBestPrice"]/text()').extract_first()
        origin_price = response.selector.xpath('//*[@class="skuListPrice"]/text()').extract_first()
        
        percent, discount, is_discount = self.discount_values(origin_price, best_price)
        data = {
            'name': name,
            'best_price': best_price,
            'origin_price': origin_price,
            'discount': discount,
            'percent': percent,
            'is_discount': is_discount,
        }

        if is_discount:
            self.data_discount = data
        
        yield data

    def float_format(self, value):
        if value is None:
            return 0.0
        value = re.sub(r'\D+', '', value)
        return float(f"{value[:-2]}.{value[-2:]}")

    def discount_values(self, origin, best):
        origin = self.float_format(origin)
        best = self.float_format(best)
        if origin > 0:
            total_discount = '%.2f%%' %  ((best*100)/origin)
            value = 'R$ %.2f' % (origin - best)
            return (total_discount, value, True)
        return ('0%', 0.0, False)

    @classmethod
    def from_crawler(cls, crawler):
        spider = cls()
        crawler.signals.connect(spider.spider_closed, signals.spider_closed)
        return spider

    def spider_closed(self, spider):
        data = self.data_discount
        if data:
            message = f"""
                Produto: {data['name']}
                Desconto: {data['percent']}
                Valor Original: {data['origin_price']}
                Valor com desconto: {data['best_price']}
                Economia: {data['discount']}
            """
            mail_from = config('MAIL_FROM')
            mailer = MailSender(mailfrom=mail_from, smtphost="smtp.gmail.com", smtpport=25, smtpuser=mail_from, smtppass=config('PASSWORD'))
            return mailer.send(to=[config('MAIL_TO')], subject="Crawler (Multilaser)", body=message)
        else:
            return 'No discount'